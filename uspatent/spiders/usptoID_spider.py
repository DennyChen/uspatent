# coding=UTF-8
import scrapy
import json
import pymongo
from pymongo import MongoClient
import csv
import time
import logging

class USPTOSpider(scrapy.Spider):
    name = "uspto_id_spider"

    def start_requests(self, start=0, end=0):       
        # urls = ['http://patft.uspto.gov/netacgi/nph-Parser?Sect1=PTO2&Sect2=HITOFF&u=%2Fnetahtml%2FPTO%2Fsearch-adv.htm&r=0&f=S&l=50&d=PTXT&RS=TTL%2F%22virtual+reality%22+OR+ABST%2F%22virtual+reality%22&Query=TTL%2F%22virtual+reality%22+OR+ABST%2F%22virtual+reality%22&TD=1003&Srch1=%22virtual+reality%22.TI.&Srch2=%22virtual+reality%22.ABTX.&Conj1=OR&StartAt=Jump+To&StartNum='+str(i) for i in range(int(self.start), int(self.end), 50)]
        
        # Goldman Sanches
        # urls = ['http://patft.uspto.gov/netacgi/nph-Parser?Sect1=PTO2&Sect2=HITOFF&u=%2Fnetahtml%2FPTO%2Fsearch-adv.htm&r=0&f=S&l=50&d=PTXT&RS=AANM%2F%22Goldman+Sachs%22&Query=AANM%2F%22Goldman+Sachs%22&TD=1003&Srch1=%22Goldman+Sachs%22.AANM.&StartAt=Jump+To&StartNum='+str(i) for i in range(int(self.start), int(self.end), 50)]


        #Morgan Stanely patent application
        # urls = ['http://appft.uspto.gov/netacgi/nph-Parser?Sect1=PTO2&Sect2=HITOFF&u=%2Fnetahtml%2FPTO%2Fsearch-adv.html&r=0&f=S&l=50&d=PG01&OS=AANM%2F%22Morgan+Stanley%22&RS=AANM%2F%22Morgan+Stanley%22&TD=197&Srch1=%28%2522Morgan+Stanley%2522.AANM.%29&StartAt=Jump+To&StartNum='+str(i) for i in range(int(self.start), int(self.end), 50)]
        # urls = ['http://patft.uspto.gov/netacgi/nph-Parser?Sect1=PTO2&Sect2=HITOFF&u=%2Fnetahtml%2FPTO%2Fsearch-adv.htm&r=0&f=S&l=50&d=PTXT&RS=AANM%2F%22Morgan+Stanley%22&Query=AANM%2F%22Morgan+Stanley%22&TD=1003&Srch1=%22Morgan+Stanley%22.AANM.&StartAt=Jump+To&StartNum='+str(i) for i in range(int(self.start), int(self.end), 50)]

        # Barclays Bank Plc
        # urls = ['http://patft.uspto.gov/netacgi/nph-Parser?Sect1=PTO2&Sect2=HITOFF&u=%2Fnetahtml%2FPTO%2Fsearch-adv.htm&r=0&f=S&l=50&d=PTXT&RS=AANM%2F%22Barclays+Bank+Plc%22&Query=AANM%2F%22Barclays+Bank+Plc%22&TD=1003&Srch1=%22Barclays+Bank+Plc%22.AANM.&StartAt=Jump+To&StartNum='+str(i) for i in range(int(self.start), int(self.end), 50)]
        # urls = ['http://appft.uspto.gov/netacgi/nph-Parser?Sect1=PTO2&Sect2=HITOFF&u=%2Fnetahtml%2FPTO%2Fsearch-adv.html&r=0&f=S&l=50&d=PG01&OS=AANM%2F%22Barclays+Bank+Plc%22&RS=AANM%2F%22Barclays+Bank+Plc%22&TD=197&Srch1=%28%2522Barclays+Bank+Plc%2522.AANM.%29&StartAt=Jump+To&StartNum='+str(i) for i in range(int(self.start), int(self.end), 50)]

        # Citicorp
        # urls = ['http://patft.uspto.gov/netacgi/nph-Parser?Sect1=PTO2&Sect2=HITOFF&u=%2Fnetahtml%2FPTO%2Fsearch-adv.htm&r=0&f=S&l=50&d=PTXT&RS=AANM%2F%22Citicorp%22&Query=AANM%2F%22Citicorp%22&TD=1003&Srch1=%22Citicorp%22.AANM.&StartAt=Jump+To&StartNum='+str(i) for i in range(int(self.start), int(self.end), 50)]
        # urls = ['http://appft.uspto.gov/netacgi/nph-Parser?Sect1=PTO2&Sect2=HITOFF&u=%2Fnetahtml%2FPTO%2Fsearch-adv.html&r=0&f=S&l=50&d=PG01&OS=AANM%2F%22Citicorp%22&RS=AANM%2F%22Citicorp%22&TD=197&Srch1=%28%2522Citicorp%2522.AANM.%29&StartAt=Jump+To&StartNum='+str(i) for i in range(int(self.start), int(self.end), 50)]

        # Bank of America Bank+of+America
        urls = ['http://appft.uspto.gov/netacgi/nph-Parser?Sect1=PTO2&Sect2=HITOFF&u=%2Fnetahtml%2FPTO%2Fsearch-adv.html&r=0&f=S&l=50&d=PG01&Query=AANM%2F%22Bank+of+America%22&p='+str(i) for i in range(int(self.start), int(self.end))]
        # urls = ['http://appft.uspto.gov/netacgi/nph-Parser?Sect1=PTO2&Sect2=HITOFF&u=%2Fnetahtml%2FPTO%2Fsearch-adv.html&r=0&f=S&l=50&d=PG01&OS=AANM%2F%22Bank+Of+America%22&RS=AANM%2F%22Bank+Of+America%22&TD=197&Srch1=%28%2522Bank+Of+America%2522.AANM.%29&StartAt=Jump+To&StartNum='+str(i) for i in range(int(self.start), int(self.end), 50)]
        # urls = ['http://patft.uspto.gov/netacgi/nph-Parser?Sect1=PTO2&Sect2=HITOFF&u=%2Fnetahtml%2FPTO%2Fsearch-adv.htm&r=0&f=S&l=50&d=PTXT&RS=AANM%2F%22Bank+of+America%22&Query=AANM%2F%22Bank+of+America%22&TD=1003&Srch1=%22Bank+of+America%22.AANM.&StartAt=Jump+To&StartNum='+str(i) for i in range(int(self.start), int(self.end), 50)]

        #JPMorgan-app
        # urls = ['http://appft.uspto.gov/netacgi/nph-Parser?Sect1=PTO2&Sect2=HITOFF&u=%2Fnetahtml%2FPTO%2Fsearch-adv.html&r=0&f=S&l=50&d=PG01&OS=AANM%2F%22JPMorgan%22&RS=AANM%2F%22JPMorgan%22&TD=197&Srch1=%28%2522JPMorgan%2522.AANM.%29&StartAt=Jump+To&StartNum='+str(i) for i in range(int(self.start), int(self.end), 50)]

        # JPMorgan
        # urls = ['http://patft.uspto.gov/netacgi/nph-Parser?Sect1=PTO2&Sect2=HITOFF&u=%2Fnetahtml%2FPTO%2Fsearch-adv.htm&r=0&f=S&l=50&d=PTXT&RS=AANM%2F%22JPMorgan%22&Query=AANM%2F%22JPMorgan%22&TD=1003&Srch1=%22JPMorgan%22.AANM.&StartAt=Jump+To&StartNum='+str(i) for i in range(int(self.start), int(self.end), 50)]

        # Bank+Of+America+Corporation
        # urls =['http://patft.uspto.gov/netacgi/nph-Parser?Sect1=PTO2&Sect2=HITOFF&u=%2Fnetahtml%2FPTO%2Fsearch-adv.htm&r=0&f=S&l=50&d=PTXT&RS=AANM%2F%22Bank+Of+America+Corporation%22&Query=AANM%2F%22Bank+Of+America+Corporation%22&TD=1003&Srch1=%22Bank+Of+America+Corporation%22.AANM.&StartAt=Jump+To&StartNum='+str(i) for i in range(int(self.start), int(self.end), 50)]
        
        # urls = ['https://www.google.com.tw/patents/US'+str(pnumber)+'?dq='+str(pnumber)+'&hl=en-us'\
        #          for pnumber in range(9000000, 9000100)]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)
            #time.sleep(10)    
        # logging.warning(urls)
    def parse(self, response):
        fromNo = int(response.url.split('=')[-1])
        print(fromNo)
        # with open('./result/patentlist_uspto/'+str(fromNo)+'.csv', 'w') as csvfile:
        #     fieldnames = ['PATNO', 'TITLE']
        #     writer = csv.DictWriter(csvfile, fieldnames = fieldnames)
        #     writer.writeheader()
        tditems = response.css('tr td a::text').extract()
        tempdict = {'PATNO':'', 'TITLE':''}
        templist = {'PAGE':fromNo/50+1, 'PATNOS':[]}
        for i in range(100):
            if i%2 == 0:
                tempdict['PATNO'] = tditems[i]
            else:
                tempdict['TITLE'] = " ".join(tditems[i].replace("\n", "").split())
                # writer.writerow(tempdict)
                templist['PATNOS'].append(tempdict)
                yield tempdict
                tempdict = {'PATNO':'', 'TITLE':''}

            



